/*
Теоретичні питання:

1. Які існують типи даних у Javascript?
  Number
  String
  Boolean
  Null
  Undefined
  Object
  Symbol
  BigInt

2. У чому різниця між == і ===?
  Оператор рівності (==) пробує перетворити значення змінних до одного типу даних і далі проводить операцію рівності, тобто: 
  0 == false // true (число 0 рівно Number(false))
  Оператор строгої рівності (===) спочатку перевіряє значення змінних на однаковий тип даних і далі проводить операцію рівності, тобто:
  0 === false // false (тип даних number не рівно типу даних boolean)

3. Що таке оператор?
  Оператор в JS - це спеціальний знак, в який закладено певні дії. Розрізняють оператори за типами і за пріоритетністю виконання.
*/

// Реалізувати просту програму на Javascript, яка взаємодіятиме з користувачем за допомогою модальних вікон браузера - alert, prompt, confirm. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

let userName;
  while (Boolean(userName) === false || Number(userName) === 0 || Number(!isNaN(userName))) {
    userName = prompt("Введіть ім'я", userName).trim();
  }

let userAge;
  while (isNaN(userAge) || userAge === 0 || userAge === null || userAge > 120) {
    userAge = prompt("Вкажіть вік", userAge);
  }

if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge <= 22) {
    const messageConfirm = confirm("Are you sure you want to continue?");
    console.log(messageConfirm);
      if (messageConfirm) {
        alert(`Welcome, ${userName}`);
      } else {
        alert("You are not allowed to visit this website");
    }
} else {
  alert(`Welcome, ${userName}`);
}